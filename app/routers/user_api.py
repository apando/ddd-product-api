from typing import Annotated
from fastapi import APIRouter, Depends, HTTPException
from app.modules.user.application.create.user_creator import UserCreator
from app.modules.user.application.find.user_finder import UserFinder

from app.modules.user.domain.user import User, UserIn, UserOut
from app.modules.user.infrastructure.persistance.user_repository_local import (
    UserRepositoryLocal,
)

from app.security import get_password_hash
from app.dependencies import get_current_active_user


user_local_repository = UserRepositoryLocal()
user_creator = UserCreator(repository=user_local_repository)
user_finder = UserFinder(repository=user_local_repository)


user_router = APIRouter(prefix="/users", tags=["Users"])


@user_router.post("/registration/", response_model=UserOut)
async def create_user(user_info: UserIn):
    """
    Create a new user.

    Parameters:
        user (User): The User object containing user details.

    Returns:
        UserOut: The newly created User object.

    """
    user = User(**user_info.model_dump())
    user.password = get_password_hash(user.password)

    user_creator.create(user)

    return user


@user_router.get("/me", response_model=UserOut)
async def read_users_me(
    current_user: Annotated[User, Depends(get_current_active_user)]
):
    """
    Get the details of the currently authenticated user.

    Parameters:
        current_user (User): The authenticated User object obtained from the access token.

    Returns:
        UserOut: The UserOut object representing the current user's details.

    """
    return current_user


@user_router.get("/{username}", response_model=UserOut)
async def read_user(username: str):
    """
    Retrieve a user with the provided username.

    This endpoint allows clients to fetch a user's details using the user's username.
    If a user with the provided username is not found, an HTTPException is raised with status code 400.

    Parameters:
        username (str): The username of the user to be retrieved.

    Returns:
        UserOut: The user's details if a user with the given username exists.

    Raises:
        HTTPException: An exception with status code 404 and a detail message of "User not found"
                       is raised if no user with the given username exists.

    """
    user = user_finder.execute(username)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")

    return user
