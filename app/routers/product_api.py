from typing import Annotated, List

from fastapi import APIRouter, Depends, HTTPException

from app.dependencies import get_current_active_user
from app.modules.product.application.create.product_creator import ProductCreator
from app.modules.product.application.find.product_finder import ProductFinder
from app.modules.product.application.search_by_criteria.product_by_criteria_searcher import (
    ProductByCriteriaSearcher,
)
from app.modules.product.domain.product import Product, ProductOut
from app.modules.product.domain.product_not_found import ProductNotFound
from app.modules.product.infrastructure.persistance.product_repository_local import (
    ProductRepositoryLocal,
)
from app.modules.user.domain.user import User


product_local_repository = ProductRepositoryLocal()

product_creator = ProductCreator(repository=product_local_repository)
product_finder = ProductFinder(repository=product_local_repository)
product_by_criteria_searcher = ProductByCriteriaSearcher(
    repository=product_local_repository
)


product_router = APIRouter(prefix="/products", tags=["Products"])


@product_router.post("/", response_model=ProductOut)
async def create_product(
    product: Product, current_user: Annotated[User, Depends(get_current_active_user)]
):
    product.owner = current_user.id
    new_product = product_creator.create(product)

    return new_product


@product_router.get("/mine", response_model=List[ProductOut])
async def get_products(current_user: Annotated[User, Depends(get_current_active_user)]):
    products = product_by_criteria_searcher.search(owner=current_user.id)
    return products


@product_router.get(
    "/{product_id}",
    response_model=ProductOut,
    dependencies=[Annotated[User, Depends(get_current_active_user)]],
)
async def read_product(product_id: str):
    try:
        product = product_finder.execute(product_id)
        return product
    except ProductNotFound as e:
        raise HTTPException(status_code=404, detail=e.errorMessage())


# @product_router.delete("/{product_id}")
# async def delete_product(
#     product_id: str, current_user: Annotated[User, Depends(get_current_active_user)]
# ):
#     product = None

#     try:
#         product = product_finder.execute(product_id)
#     except ProductNotFound as e:
#         raise HTTPException(status_code=404, detail=e.errorMessage())

#     if product.owner != current_user.id:
#         raise HTTPException(status_code=404, detail="Not authorized")

#     product_service.delete_product(product_id)

#     return {"detail": "Product deleted"}


# def save_upload_file(upload_file: UploadFile, destination: Path) -> None:
#     """
#     Helper function to save the uploaded file to a destination.

#     Parameters:
#         upload_file (UploadFile): The file uploaded by the client.
#         destination (Path): The path where the file should be saved.

#     """
#     try:
#         with destination.open("wb") as buffer:
#             shutil.copyfileobj(upload_file.file, buffer)
#     finally:
#         upload_file.file.close()


# @product_router.post("/{product_id}/images", response_model=ProductOut)
# async def read_product(
#     product_id: str,
#     current_user: Annotated[User, Depends(get_current_active_user)],
#     images: List[UploadFile] = File(...),
# ):
#     product = product_service.get_product(product_id)
#     if not product:
#         raise HTTPException(status_code=404, detail="Product not found")

#     if product.owner != current_user.id:
#         raise HTTPException(status_code=401, detail="Not authorized")

#     # TODO: this is a good hint
#     # with open(f"{file.filename}", "wb") as buffer:
#     #     shutil.copyfileobj(file.file, buffer)
#     # images_db[file.filename] = f"{file.filename}"
#     # products_db[product.name] = {"name": product.name, "price": product.price, "description": product.description, "images": images_db[file.filename], "owner": current_user.username}

#     uploaded_images = ImageService.upload_images(images, bucket="products")
#     updates = {images: uploaded_images}

#     product = product_service.update_product(product_id, updates)
#     return product
