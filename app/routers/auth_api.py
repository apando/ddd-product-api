from typing import Annotated
from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm

from app.modules.auth.application.authenticate.user_authenticator import (
    UserAuthenticator,
)
from app.modules.user.infrastructure.persistance.user_repository_local import (
    UserRepositoryLocal,
)

from app.security import create_access_token


user_local_repository = UserRepositoryLocal()
user_authenticator = UserAuthenticator(repository=user_local_repository)


auth_router = APIRouter(tags=["Auth"])


@auth_router.post("/token")
def login_for_access_token(form_data: Annotated[OAuth2PasswordRequestForm, Depends()]):
    """
    Generate an access token for a user.

    This function handles the POST /token route. It tries to authenticate the
    user using the provided username and password in the form_data. If the
    authentication is successful, it creates an access token for the user
    and returns it along with the token type.

    Args:
        form_data (Annotated[OAuth2PasswordRequestForm, Depends()]): The form data
        which should include the 'username' and 'password' fields for user
        authentication. FastAPI dependency injection is used to validate the
        form data.

    Returns:
        dict: A dictionary containing the 'access_token' and 'token_type' ('bearer').

    Raises:
        HTTPException: If authentication fails, an HTTPException with status code 401
        (Unauthorized) is raised, along with a detail message indicating incorrect
        username or password. 'WWW-Authenticate: Bearer' is also added to the headers
        as per the OAuth2 specification.
    """
    user = user_authenticator.authenticate(form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )

    access_token = create_access_token(data={"sub": user.username})

    return {"access_token": access_token, "token_type": "bearer"}
