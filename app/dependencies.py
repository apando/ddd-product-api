from typing import Annotated
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from app.modules.auth.domain.token import TokenData
from app.modules.user.domain.user import User
from app.modules.user.infrastructure.persistance.user_repository_local import (
    UserRepositoryLocal,
)

from app.security import decode_token


user_local_repository = UserRepositoryLocal()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


def get_current_user(token: Annotated[str, Depends(oauth2_scheme)]):
    """
    Get the current authenticated user based on the provided access token.

    Parameters:
        token (str): The access token provided for authentication.

    Returns:
        User: The User object representing the current authenticated user.

    Raises:
        HTTPException: If the provided token is invalid or cannot be validated,
                       a 401 UNAUTHORIZED HTTPException will be raised.

    """
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )

    try:
        payload = decode_token(token)
        username: str = payload.get("sub")

        if username is None:
            raise credentials_exception

        token_data = TokenData(username=username)
    except Exception:
        raise credentials_exception

    user = user_local_repository.search(token_data.username)
    if user is None:
        raise credentials_exception

    return user


async def get_current_active_user(
    current_user: Annotated[User, Depends(get_current_user)]
):
    if current_user.disabled:
        raise HTTPException(status_code=400, detail="Inactive user")
    return current_user
