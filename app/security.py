from datetime import datetime, timedelta
from jose import jwt, JWTError
from passlib.context import CryptContext
from typing import Optional

SECRET_KEY = "your-secret-key"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30


def create_access_token(data: dict, expires_delta: timedelta = None):
    """
    Create an access token with the provided data and expiration time.

    Parameters:
        data (dict): The payload data to be encoded into the token.
        expires_delta (timedelta, optional): The time duration until the token expires.
                                             If not provided, a default expiration time
                                             of 15 minutes will be used.

    Returns:
        str: The encoded access token.

    """
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


def decode_token(token: str) -> Optional[dict]:
    """
    Decode an access token and extract the payload data.

    Parameters:
        token (str): The encoded access token to be decoded.

    Returns:
        dict or None: The payload data if the token is valid, otherwise returns None.

    """
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        return payload
    except JWTError:
        return None


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

def get_password_hash(password):
    """
    Hash the given password using bcrypt.

    Parameters:
        password (str): The password to be hashed.

    Returns:
        str: The hashed password.

    """
    return pwd_context.hash(password)


def verify_password(plain_password, hashed_password):
    """
    Verify if the given plain password matches the hashed password.

    Parameters:
        plain_password (str): The plain password to be verified.
        hashed_password (str): The hashed password to compare against.

    Returns:
        bool: True if the plain password matches the hashed password,
              otherwise False.

    """
    return pwd_context.verify(plain_password, hashed_password)
