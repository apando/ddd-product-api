import uvicorn

from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles

from .routers.auth_api import auth_router
from .routers.product_api import product_router
from .routers.user_api import user_router

app = FastAPI()

app.include_router(auth_router)
app.include_router(product_router)
app.include_router(user_router)

app.mount("/static", StaticFiles(directory="static"), name="static")

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
