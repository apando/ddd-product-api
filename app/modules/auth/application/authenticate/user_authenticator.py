from app.modules.user.domain.user_repository import UserRepositoryABC
from app.security import verify_password


class UserAuthenticator:
    def __init__(self, repository: UserRepositoryABC) -> None:
        self.repository = repository

    def authenticate(self, username: str, password: str):
        """
        Authenticate a user based on the provided username and password.

        This method retrieves the user details from the repository based on the
        provided username. Then, it verifies the provided password with the
        stored password for the user. If the verification is successful, it
        returns the user. If the user doesn't exist or the password doesn't
        match, it returns False.

        Args:
            username (str): The username of the user to be authenticated.
            password (str): The password provided by the user for authentication.

        Returns:
            User: The authenticated user if the username and password match.
            False otherwise.

        """
        user = self.repository.search(username)
        if not user:
            return False

        if not verify_password(password, user.password):
            return False

        return user
