from enum import Enum, auto


class OrderType(Enum):
    """
    OrderType Enum contains the order types that can be applied to a collection of data.

    @method:
        asc: Ascending order.
        desc: Descending order.
        none: No order applied.

    Each of these can be accessed via dot notation (e.g., OrderType.asc, OrderType.desc, OrderType.none)
    """

    ASC = auto()
    DESC = auto()
    NONE = auto()

    def is_none(self):
        """
        Check if the order type is NONE.

        Returns:
            bool: True if the order type is NONE, False otherwise.
        """
        return self == OrderType.NONE

    @staticmethod
    def throw_exception_for_invalid_value(value):
        """
        Throws an exception for an invalid value. This method should be invoked if an invalid value is used
        as an order type.

        Args:
            value (str): The invalid value.

        Raises:
            ValueError: Always, because the value is invalid.
        """
        raise ValueError(f"The order type {value} is invalid")
