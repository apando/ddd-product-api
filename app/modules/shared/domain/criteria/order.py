from app.modules.shared.domain.criteria.order_type import OrderType


class Order:
    """
    Order class encapsulates the information about how a collection of data should be ordered.

    An Order consists of an OrderBy attribute, which specifies the field to be ordered,
    and an OrderType, which specifies the type of ordering (ascending, descending, or none).

    A serialized Order instance is represented as a string in the format "orderBy.orderType".
    """

    def __init__(self, order_by: str, order_type: OrderType):
        """
        Initialize an Order instance.

        Args:
            order_by (OrderBy): The field to be ordered.
            order_type (OrderType): The type of ordering (ascending, descending, or none).
        """
        self.order_by = order_by
        self.order_type = order_type

    @classmethod
    def create_desc(cls, order_by):
        """
        Create an Order instance with descending order.

        Args:
            order_by (OrderBy): The field to be ordered.

        Returns:
            Order: An Order instance with the specified order_by and descending order.
        """
        return cls(order_by, OrderType.DESC)

    @classmethod
    def from_values(cls, order_by: str = None, order=None):
        """
        Create an Order instance from provided values.

        Args:
            order_by (str, optional): The field to be ordered.
            order (str, optional): The type of ordering (ascending, descending, or none).

        Returns:
            Order: An Order instance with the specified order_by and order_type.
                  If no order_by is provided, it returns an Order with no ordering.
        """
        return cls(order_by, OrderType(order)) if order_by else cls.none()

    @staticmethod
    def none():
        """
        Create an Order instance with no ordering.

        Returns:
            Order: An Order instance with no ordering.
        """
        return Order("", OrderType.NONE)

    def order_by(self):
        """
        Get the OrderBy field of the Order.

        Returns:
            OrderBy: The OrderBy field of the Order.
        """
        return self.order_by

    def order_type(self):
        """
        Get the OrderType of the Order.

        Returns:
            OrderType: The OrderType of the Order.
        """
        return self.order_type

    def is_none(self):
        """
        Check if the Order is none.

        Returns:
            bool: True if the Order has no ordering, False otherwise.
        """
        return self.order_type == None

    def serialize(self):
        """
        Serialize the Order into a string.

        Returns:
            str: A string in the format "orderBy.orderType".
        """
        return f"{self.order_by}.{self.order_type.value}"
