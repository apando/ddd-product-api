from app.modules.shared.domain.criteria.filter_operator import FilterOperator


class Filter:
    """
    Filter represents a filter that can be applied to a collection of data. A filter consists of a field,
    an operator, and a value.
    """

    def __init__(self, field: str, operator: FilterOperator, value: str):
        self.field = field
        self.operator = operator
        self.value = value

    @classmethod
    def from_values(cls, values: dict):
        """
        Creates a new Filter instance from a dictionary of values.

        Args:
            values (dict): A dictionary containing the field, operator, and value for the filter.

        Returns:
            Filter: A new Filter instance.
        """
        return cls(
            values["field"],
            FilterOperator(values["operator"]),
            values["value"],
        )

    def serialize(self):
        """
        Serializes the Filter instance into a string format.

        Returns:
            string: A string representation of the Filter instance.
        """
        return f"{self.field}.{self.operator.value}.{self.value}"
