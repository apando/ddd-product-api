from ast import List
from functools import reduce
from logging import Filter


class Filters(List[Filter]):
    """
    Filters is a collection of Filter objects.
    """

    @classmethod
    def from_values(cls, values: List[dict]):
        """
        Creates a new Filters instance from a list of dictionaries, where each dictionary contains
        the values for a Filter.

        Args:
            values (List[dict]): A list of dictionaries, each containing the field, operator, and value for a Filter.

        Returns:
            Filters: A new Filters instance.
        """
        return cls([Filter.from_values(value) for value in values])

    def add(self, filter: Filter):
        """
        Adds a new Filter to the Filters collection.

        Args:
            filter (Filter): The Filter to add.

        Returns:
            Filters: The Filters instance, after the new Filter has been added.
        """
        self.append(filter)
        return self

    def filters(self):
        """
        Gets the list of Filter objects in the Filters collection.

        Returns:
            list[Filter]: The list of Filter objects.
        """
        return self

    def serialize(self):
        """
        Serializes the Filters instance into a string format.

        Returns:
            string: A string representation of the Filters instance.
        """
        return reduce(
            lambda accumulate, filter: f"{accumulate}^{filter.serialize()}", self, ""
        )

    @property
    def type(self):
        """
        Gets the type of the elements in the Filters collection.

        Returns:
            type: The type of the elements in the Filters collection.
        """
        return Filter
