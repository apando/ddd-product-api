from typing import Optional, List

from app.modules.shared.domain.criteria.filters import Filters
from app.modules.shared.domain.criteria.order import Order


class Criteria:
    filters: Filters
    order: Order
    offset: Optional[int]
    limit: Optional[int]

    def hasFilters(self) -> bool:
        return self.filters.count() > 0

    def hasOrder(self) -> bool:
        return not self.order.isNone()

    def plainFilters(self) -> List:
        return self.filters.filters()

    def filters(self) -> Filters:
        return self.filters

    def order(self) -> Order:
        return self.order

    def offset(self) -> Optional[int]:
        return self.offset

    def limit(self) -> Optional[int]:
        return self.limit

    def serialize(self) -> str:
        return f"{self.filters.serialize()}~~{self.order.serialize()}~~{self.offset}~~{self.limit}"
