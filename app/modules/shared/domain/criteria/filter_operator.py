from enum import Enum
from typing import Any


class FilterOperator(Enum):
    """
    FilterOperator defines the possible operators that can be used in a filter.
    """

    EQUAL = "="
    NOT_EQUAL = "!="
    GT = ">"
    LT = "<"
    CONTAINS = "CONTAINS"
    NOT_CONTAINS = "NOT_CONTAINS"
    containing = [CONTAINS, NOT_CONTAINS]

    def is_containing(self):
        """
        Checks if the filter operator is a 'containing' operator (i.e., either 'CONTAINS' or 'NOT_CONTAINS')

        Returns:
            bool: True if the operator is a 'containing' operator, False otherwise.
        """
        return self in self.containing

    @staticmethod
    def throw_exception_for_invalid_value(value):
        """
        Throws an exception for an invalid filter operator.

        Args:
            value: The invalid filter operator value.
        """
        raise ValueError(f"The filter <{value}> is invalid")
