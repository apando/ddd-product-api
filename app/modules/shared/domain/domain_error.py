from abc import ABC, abstractmethod


class DomainErrorABC(ABC, Exception):
    """Abstract base class for domain-specific errors."""

    @abstractmethod
    def __init__(self, message):
        super().__init__(message)

    def errorCode() -> str:
        pass

    def errorMessage() -> str:
        pass
