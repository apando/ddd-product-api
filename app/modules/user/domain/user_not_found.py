from app.modules.shared.domain.domain_error import DomainErrorABC


class UserNotFound(DomainErrorABC):
    def __init__(self, username: str):
        super()
        self.username = username

    def errorCode(self) -> str:
        return "user_not_found"

    def errorMessage(self) -> str:
        return f"The user <{self.username}> has not been found"
