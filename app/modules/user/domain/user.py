from typing import Optional
from pydantic import BaseModel, Field
import uuid


class User(BaseModel):
    id: str = Field(default_factory=lambda: str(uuid.uuid4()))
    first_name: Optional[str] = None
    last_name: Optional[str] = None
    username: str
    password: str
    disabled: bool = False


class UserIn(BaseModel):
    first_name: Optional[str] = None
    last_name: Optional[str] = None
    username: str
    password: str


class UserOut(BaseModel):
    id: str
    first_name: Optional[str] = None
    last_name: Optional[str] = None
    username: str
