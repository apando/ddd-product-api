from abc import ABC
from typing import Optional

from .user import User


class UserRepositoryABC(ABC):
    def save(self, user: User) -> User:
        pass

    def search(self, username: str) -> Optional[User]:
        pass

    def search_by_criteria(self, criteria: str) -> Optional[User]:
        pass
