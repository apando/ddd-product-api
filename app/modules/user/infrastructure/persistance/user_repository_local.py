from typing import Optional

from ...domain.user import User
from ...domain.user_repository import UserRepositoryABC


# fake database
users_db = {
    "demo_user": {
        "id": "895f77f5-23f8-48c2-b6da-07c9f00b7846",
        "first_name": "Demo",
        "last_name": "User",
        "username": "demo_user",
        "password": "$2b$12$nPa5tV/5GfYzqclB2YKND.Wn8wYErVgHPpHs6Mkg3mzc.0jZHzvtW"
        # demo_password
    },
}


class UserRepositoryLocal(UserRepositoryABC):
    """
    A class representing a User Repository, responsible for managing users.

    Methods:
        get_user_by_username(self, username: str) -> Optional[User]:
            Get a specific user by its unique username.

        create_user(self, user: User) -> UserOut:
            Add a new user to the repository.

    """

    def search(self, username: str) -> Optional[User]:
        """
        Get a user by their username.

        Parameters:
            username (str): The username of the user to retrieve.

        Returns:
            Optional[User]: If a user with the given username exists in the database,
                            returns the user as a User object. Otherwise, returns None.

        """
        if username in users_db:
            user_data = users_db[username]
            return User(**user_data)

        return None

    def save(self, user: User) -> User:
        """
        Create a new user and add it to the user database.

        Parameters:
            user (User): The User object containing user details.

        Returns:
            UserOut: The UserOut object representing the newly created user.

        """
        users_db[user.username] = {
            "id": user.id,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "username": user.username,
            "password": user.password,
        }
        return user
