from app.modules.user.domain.user import User
from app.modules.user.domain.user_not_found import UserNotFound
from app.modules.user.domain.user_repository import UserRepositoryABC


class UserFinder:
    def __init__(self, repository: UserRepositoryABC) -> None:
        self.repository = repository

    def execute(self, username: str) -> User:
        """
        Retrieve a user from the repository based on the given username.

        This function uses the username to search the repository for a corresponding
        user. If a user with the given username exists, the function returns the user object.

        Parameters:
            username (str): The username of the user to retrieve.

        Returns:
            User: The user object if a user with the given username exists in the repository.
            None: If no user with the given username exists in the repository.

        """
        user = self.repository.search(username)
        if not user:
            raise UserNotFound(username)

        return user
