from app.modules.user.domain.user import User
from app.modules.user.domain.user_repository import UserRepositoryABC


class UserCreator:
    def __init__(self, repository: UserRepositoryABC) -> None:
        self.repository = repository

    def create(self, user: User):
        """
        Create a new user.

        Parameters:
            user (User): The User object containing user details to be created.
        """
        self.repository.save(user)
