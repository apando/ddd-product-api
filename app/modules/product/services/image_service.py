from app.repositories.image_repository import ImageRepository
from fastapi import UploadFile
from typing import List


class ImageService:
    @staticmethod
    async def upload_images(images: List[UploadFile], bucket: str):
        image_paths = []

        for image in images:
            image_path = await ImageRepository.save_image(image, bucket)
            image_paths.append(image_path)

        return image_paths
