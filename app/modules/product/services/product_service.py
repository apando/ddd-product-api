from typing import List, Optional
from app.models.product import Product, ProductOut
from app.repositories.product_repository import ProductRepository


class ProductService:
    """
    A service class to manage product-related operations.

    This class provides methods to interact with a ProductRepository and perform
    various CRUD (Create, Read, Update, Delete) operations on products.

    Attributes:
        repository (ProductRepository): The ProductRepository used for product-related operations.

    Methods:
        create_product(product: Product) -> Optional[Product]:
            Create a new product.

        delete_product(product_id: str) -> Optional[Product]:
            Delete a product by its unique identifier.

        get_product(product_id: str) -> Optional[Product]:
            Get a product by its unique identifier.

        get_owner_products(owner: str) -> List[Product]:
            Get all products owned by a specific owner.

        get_all_products() -> List[Product]:
            Get all products available.

    """

    def __init__(self, repository: ProductRepository):
        """
        Initialize the ProductService with a ProductRepository.

        Parameters:
            repository (ProductRepository): The ProductRepository used for product-related operations.

        """
        self.repository = repository

    def create_product(self, product: Product) -> Optional[Product]:
        """
        Create a new product.

        Parameters:
            product (Product): The Product object containing product details to be created.

        Returns:
            Optional[Product]: The newly created Product object if successful, otherwise None.

        """
        return self.repository.create_product(product)

    def get_product(self, product_id: str) -> Optional[Product]:
        """
        Get a product by its unique identifier.

        Parameters:
            product_id (str): The unique identifier of the product to retrieve.

        Returns:
            Optional[Product]: The Product object representing the product if found, otherwise None.

        """
        return self.repository.get_product(product_id)

    def update_product(self, product_id: str, updates: dict) -> Product:
        return self.repository.update_product(product_id, updates)

    def delete_product(self, product_id: str) -> Optional[Product]:
        """
        Delete a product by its unique identifier.

        Parameters:
            product_id (str): The unique identifier of the product to be deleted.

        Returns:
            Optional[Product]: The Product object representing the deleted product if found, otherwise None.

        """
        return self.repository.delete(product_id)

    def get_owner_products(self, owner: str) -> List[Product]:
        """
        Get all products owned by a specific owner.

        Parameters:
            owner (str): The owner's name.

        Returns:
            List[Product]: A list of Product objects representing the products owned by the owner.

        """
        return self.repository.get_owner_products(owner)

    def get_all_products(self) -> List[Product]:
        """
        Get all products available.

        Returns:
            List[Product]: A list of Product objects representing all the products.

        """
        return self.repository.get_all_products()
