from typing import List, Optional

from app.modules.product.domain.product import Product
from app.modules.product.domain.product_repository import ProductRepositoryABC
from app.modules.shared.domain.criteria.criteria import Criteria

# TODO: generate more products, use more real data and adding id prop set with a uuid value
products_db = {
    "c4850b29-7ecf-42a4-9427-f7f630882413": {
        "id": "c4850b29-7ecf-42a4-9427-f7f630882413",
        "name": "iPhone 12",
        "price": 799.99,
        "description": "Apple iPhone 12 with Super Retina XDR display and A14 Bionic chip.",
        "images": {},
        "owner": "895f77f5-23f8-48c2-b6da-07c9f00b7846",
    },
    "14f9e0a0-7431-42f2-b222-826a34b16e66": {
        "id": "14f9e0a0-7431-42f2-b222-826a34b16e66",
        "name": "Samsung Galaxy S21",
        "price": 899.00,
        "description": "Samsung Galaxy S21 with Dynamic AMOLED 2X display and Exynos 2100/Snapdragon 888 processor.",
        "images": {},
        "owner": "895f77f5-23f8-48c2-b6da-07c9f00b7846",
    },
    "821d754d-82c1-4b13-97c5-e20426ebef05": {
        "id": "821d754d-82c1-4b13-97c5-e20426ebef05",
        "name": "Sony WH-1000XM4",
        "price": 349.99,
        "description": "Sony WH-1000XM4 wireless noise-canceling headphones with industry-leading noise cancellation technology.",
        "images": {},
        "owner": "895f77f5-23f8-48c2-b6da-07c9f00b7846",
    },
}


class ProductRepositoryLocal(ProductRepositoryABC):
    def search(self, product_id: str) -> Optional[Product]:
        """
        Get a specific product by its unique identifier.

        Parameters:
            product_id (str): The unique identifier of the product.

        Returns:
            Optional[Product]: If the product exists, returns the product details
                                 as a Product object. Otherwise, returns None.

        """
        if product_id in products_db:
            product_data = products_db[product_id]
            return Product(**product_data)

        return None

    def search_all_products(self) -> List[Product]:
        """
        Get all products available in the product database.

        Returns:
            List[Product]: A list of Product objects, representing all the products.

        """
        return [Product(**data) for _, data in products_db.items()]

    def search_by_criteria(self, criteria: Criteria) -> List[Product]:
        """
        Get all products owned by a specific owner.

        Parameters:
            owner (str): The owner's unique identifier.

        Returns:
            List[Product]: A list of Product objects representing the products owned by the owner.

        """

        a = [for field in criteria.fields()]

        return [
            Product(**product)
            for _, product in products_db.items()
            if product["owner"] == owner
        ]

    def save(self, product: Product) -> Product:
        """
        Create a new product and add it to the product database.

        Parameters:
            product (Product): The Product object containing product details.

        Returns:
            Product: the newly created product.

        """
        products_db[product.id] = {
            "id": product.id,
            "name": product.name,
            "price": product.price,
            "description": product.description,
            "owner": product.owner,
        }

        return product

    # def update(self, product_id: str, updates: dict) -> Product:
    #     """
    #     Update an existing product in the repository with the provided data.

    #     Parameters:
    #         product_id (str): The ID of the product to be updated.
    #         updates (dict): The dictionary containing the updated product data.

    #     Returns:
    #         Product: the newly updated product

    #     """
    #     if product_id in products_db:
    #         for key, new_value in updates.items():
    #             if key != "id" and key != "owner":
    #                 products_db[product_id][key] = new_value

    #         return Product(**products_db[product_id])

    #     return None

    def delete(self, product_id: str) -> Product:
        """
        Delete a product from the product database.

        Parameters:
            product_id (str): The unique identifier of the product to be deleted.

        Returns:
            Product: The Product object that was deleted.

        Raises:
            KeyError: If the product_id does not exist in the database.

        """
        removed_item = products_db.pop(product_id)
        return removed_item
