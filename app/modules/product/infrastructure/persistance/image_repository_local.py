# repositories/image_repository.py
from fastapi import UploadFile
from uuid import uuid4
from pathlib import Path
import shutil


class ImageRepository:
    @staticmethod
    async def save_image(upload_file: UploadFile, bucket: str):
        image_directory = Path(f"static/{bucket}_images")
        image_directory.mkdir(parents=True, exist_ok=True)

        image_filename = f"{uuid4().hex}{Path(upload_file.filename).suffix}"
        image_path = image_directory / image_filename

        with image_path.open("wb") as buffer:
            shutil.copyfileobj(upload_file.file, buffer)

        return str(image_path)
