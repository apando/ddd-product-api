from abc import ABC
from typing import Optional, List

from app.modules.shared.domain.criteria.criteria import Criteria

from .product import Product


class ProductRepositoryABC(ABC):
    def save(self, product: Product) -> Product:
        pass

    def search(self, id: str) -> Optional[Product]:
        pass

    def search_by_criteria(self, criteria: Criteria) -> Optional[Product]:
        pass
