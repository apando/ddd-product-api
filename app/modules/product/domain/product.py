from ast import List
from pydantic import BaseModel, Field
from typing import Optional
import uuid


class Product(BaseModel):
    id: str = Field(default_factory=lambda: str(uuid.uuid4()))
    name: str
    price: float
    description: Optional[str] = None
    images: Optional[List[str]] = []
    owner: Optional[str] = None


class ProductOut(BaseModel):
    id: str
    name: str
    price: float
    description: Optional[str] = None
    images: Optional[List[str]] = []
