from app.modules.shared.domain.domain_error import DomainErrorABC


class ProductNotFound(DomainErrorABC):
    def __init__(self, id: str):
        super()
        self.id = id

    def errorCode(self) -> str:
        return "product_not_found"

    def errorMessage(self) -> str:
        return f"The product <{self.id}> has not been found"
