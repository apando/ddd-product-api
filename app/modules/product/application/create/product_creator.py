from app.modules.product.domain.product import Product
from app.modules.product.domain.product_repository import ProductRepositoryABC


class ProductCreator:
    def __init__(self, repository: ProductRepositoryABC) -> None:
        self.repository = repository

    def create(self, product: Product):
        """
        Create a new product.

        Parameters:
            product (Product): The Product object containing product details to be created.

        Returns:
            Optional[Product]: The newly created Product object if successful, otherwise None.

        """
        self.repository.save(product)
