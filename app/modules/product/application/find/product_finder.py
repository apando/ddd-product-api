from typing import Optional

from app.modules.product.domain.product import Product
from app.modules.product.domain.product_not_found import ProductNotFound
from app.modules.product.domain.product_repository import ProductRepositoryABC


class ProductFinder:
    def __init__(self, repository: ProductRepositoryABC) -> None:
        self.repository = repository

    def execute(self, product_id: str) -> Optional[Product]:
        """
        Get a product by its unique identifier.

        Parameters:
            product_id (str): The unique identifier of the product to retrieve.

        Returns:
            Optional[Product]: The Product object representing the product if found, otherwise None.

        """
        product = self.repository.search(product_id)
        if not product:
            raise ProductNotFound(product_id)

        return product
