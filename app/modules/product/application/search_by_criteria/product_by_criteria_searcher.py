from ast import List
from app.modules.product.domain.product import Product
from app.modules.product.domain.product_repository import ProductRepositoryABC
from app.modules.shared.domain.criteria.criteria import Criteria


class ProductByCriteriaSearcher:
    def __init__(self, repository: ProductRepositoryABC) -> None:
        self.repository = repository

    def search(self, criteria: Criteria) -> List[Product]:
        return self.repository.search_by_criteria(criteria)
