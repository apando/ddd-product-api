from fastapi.testclient import TestClient
from app.main import app  # Assuming your FastAPI app is defined in the file app/main.py
import pytest
from app.modules.user.application.find.user_finder import UserFinder

from app.modules.user.domain.user import User


@pytest.fixture
def client():
    """
    A pytest fixture that creates a new TestClient for each test.

    Returns:
        TestClient: A new TestClient instance.
    """
    return TestClient(app)


def test_create_user(client: TestClient):
    """
    Test the /users/registration/ endpoint of the API.

    This function tests the scenario where a new user is created successfully with
    a POST request to /users/registration/. It sends a valid UserIn object and
    expects a 200 status code and a UserOut object in response.

    Args:
        client (TestClient): The TestClient instance to use for the requests.
    """
    response = client.post(
        "/users/registration/",
        json={
            "first_name": "New",
            "last_name": "User",
            "username": "new_user",
            "password": "new_password",
        },
    )
    assert response.status_code == 200
    assert "id" in response.json()
    assert response.json()["username"] == "new_user"


def test_read_users_me_no_auth(client: TestClient):
    """
    Test the /users/me endpoint of the API without authentication.

    This function tests the scenario where a GET request is made to /users/me without
    an authorization header. It expects a 401 status code in response.

    Args:
        client (TestClient): The TestClient instance to use for the requests.
    """
    response = client.get("/users/me")
    assert response.status_code == 401


def test_read_users_me_with_auth(client: TestClient):
    """
    Test the /users/me endpoint of the API with authentication.

    This function tests the scenario where a GET request is made to /users/me with
    a valid authorization header. It expects a 200 status code and a UserOut object in response.

    Note: This test requires a way to generate a valid authorization token for the test user.
    This could be done by first making a request to the /token endpoint, or by using a helper
    function that generates tokens for testing.

    Args:
        client (TestClient): The TestClient instance to use for the requests.
    """
    # First, authenticate the user to get the access token
    login_response = client.post(
        "/token", data={"username": "demo_user", "password": "demo_password"}
    )

    assert login_response.status_code == 200
    token = login_response.json().get("access_token")

    response = client.get("/users/me", headers={"Authorization": f"Bearer {token}"})
    assert response.status_code == 200
    assert "id" in response.json()
    assert (
        response.json()["username"] == "demo_user"
    )  # assuming the username of the test user is "test_user"


def test_read_user(mocker, client: TestClient):
    """
    Test case for the '/users/{username}' endpoint.

    This test case checks if the endpoint responds with the correct status code
    and content when a user is found. The UserService.get_user_by_username method
    is mocked to return a User object.
    """
    # Create a mock user
    mock_user = User(
        id="some-id",
        first_name="John",
        last_name="Doe",
        username="johndoe",
        password="pass",
    )

    # Mock UserService.get_user_by_username to return the mock user
    mocker.patch.object(UserFinder, "execute", return_value=mock_user)

    # Send a GET request to the endpoint
    response = client.get("/users/johndoe")

    # Assert that the response is as expected
    assert response.status_code == 200
    assert response.json() == {
        "id": "some-id",
        "first_name": "John",
        "last_name": "Doe",
        "username": "johndoe",
    }


def test_read_user_not_found(mocker, client: TestClient):
    """
    Test case for the '/users/{username}' endpoint when user does not exist.

    This test case checks if the endpoint responds with the correct status code
    and detail message when a user is not found. The UserService.get_user_by_username
    method is mocked to return None.
    """
    # Mock UserService.get_user_by_username to return None
    mocker.patch.object(UserFinder, "execute", return_value=None)

    # Send a GET request to the endpoint
    response = client.get("/users/nonexistent")

    # Assert that the response is as expected
    assert response.status_code == 404
    assert response.json() == {
        "detail": "User not found",
    }
