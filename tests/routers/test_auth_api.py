from fastapi.testclient import TestClient
from app.main import app  # Assuming your FastAPI app is defined in the file app/main.py
import pytest


@pytest.fixture
def client():
    """
    A pytest fixture that creates a new TestClient for each test.

    Returns:
        TestClient: A new TestClient instance.
    """
    return TestClient(app)


def test_login_for_access_token_successful(client):
    """
    Test the /token endpoint of the API with correct credentials.

    This function tests the scenario where the API returns a 200 status code and an
    access token when the correct username and password are provided.

    Args:
        client (TestClient): The TestClient instance to use for the requests.
    """

    response = client.post(
        "/token",
        data={"username": "demo_user", "password": "demo_password"},
    )
    assert response.status_code == 200
    assert "access_token" in response.json()
    assert response.json()["token_type"] == "bearer"


def test_login_for_access_token_incorrect_username(client):
    """
    Test the /token endpoint of the API with incorrect username.

    This function tests the scenario where the API returns a 401 status code when an
    incorrect username is provided.

    Args:
        client (TestClient): The TestClient instance to use for the requests.
    """

    response = client.post(
        "/token",
        data={"username": "wrong_username", "password": "demo_password"},
    )
    assert response.status_code == 401
    assert "detail" in response.json()
    assert response.json()["detail"] == "Incorrect username or password"


def test_login_for_access_token_incorrect_password(client):
    """
    Test the /token endpoint of the API with incorrect password.

    This function tests the scenario where the API returns a 401 status code when an
    incorrect password is provided for a valid user.

    Args:
        client (TestClient): The TestClient instance to use for the requests.
    """

    response = client.post(
        "/token",
        data={"username": "demo_user", "password": "wrong_password"},
    )
    assert response.status_code == 401
    assert "detail" in response.json()
    assert response.json()["detail"] == "Incorrect username or password"
